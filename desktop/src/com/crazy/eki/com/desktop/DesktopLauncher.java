package com.crazy.eki.com.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.crazy.eki.com.DodgeHero;

public class DesktopLauncher {
	public static void main (String[] arg) {
		pack();
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new DodgeHero(), config);
	}

	static void pack(){
		TexturePacker.Settings settings= new TexturePacker.Settings();
		settings.maxHeight = 4096;
		settings.maxWidth  = 4096;
		settings.pot = true;
		TexturePacker.process(settings,"../../assets","packed","game_assets");
	}
}
