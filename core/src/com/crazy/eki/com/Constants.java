package com.crazy.eki.com;

import com.crazy.eki.com.resources.Resources;

public class Constants {
    public static final int TILE_SIZE = 32;
    public static final int WORLD_WIDTH = 12;
    public static final int WORLD_HEIGHT = 8;
    public static final int SCREEN_WIDTH = WORLD_WIDTH * TILE_SIZE;
    public static final int SCREEN_HEIGHT = WORLD_HEIGHT * TILE_SIZE;
    public static final int MAX_BASE_X = 3;
    public static final int MAX_BASE_Y = 3;
    public static final float WARNING_TIME = 0.5f;
    public static final float BASE_ATTACK_TIME = 2.0f;
    public static final float BONUS_SPAWN_TIME = 2.0f;
    public static final float BLINKING_CHARACTER_TIME = 0.25f;
    public static final int MAX_BONUSES_AT_FIELD = 3;

    public static final int PLAYER_HIT_POINTS = 4;
    public static final int BASE_PLAYER_DAMAGE = 1;
    public static final int DEFAULT_ENEMY_LIVES = 10;
    public static final int DEFAULT_ENEMY_DAMAGE = 2;
    public static final float DEFAULT_PLAYER_APPROACH_TIME = 0.7f;
    public static final float DEFAULT_ENEMY_SCALE_TIME = 0.7f;

}
