package com.crazy.eki.com.graph;

import static com.crazy.eki.com.Constants.TILE_SIZE;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.crazy.eki.com.resources.Resources;


public class Background {
    public Background() {

    }

    public void draw(Stage _gameStage, Resources resources) {
        final Batch batch = _gameStage.getBatch();
        batch.begin();
        for (int x = 0; x < _gameStage.getWidth(); x += TILE_SIZE) {
            for (int y = 0; y < _gameStage.getHeight(); y += TILE_SIZE) {
                batch.draw(resources.grass, x, y, 0, 0, TILE_SIZE, TILE_SIZE, 1.01F, 1.01F, 0);
            }
        }
        for (int x = 0; x < _gameStage.getWidth(); x += TILE_SIZE) {
            batch.draw(resources.deepWater, x, _gameStage.getHeight()-TILE_SIZE, 0, 0, TILE_SIZE, TILE_SIZE, 1.01F, 1.01F, 0);
            batch.draw(resources.deepWater, x, 0, 0, 0, TILE_SIZE, TILE_SIZE, 1.01F, 1.01F, 0);

        }
        batch.end();
    }
}

