package com.crazy.eki.com.graph.effects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Pool;
import com.crazy.eki.com.graph.SizeEvaluator;
import com.crazy.eki.com.graph.effects.EffectEngine;

public abstract class Effect implements Pool.Poolable {

    protected boolean isAlive;
    protected float timeAlive;

    public Effect() {
        isAlive = false;
        this.timeAlive = 0f;
    }

    @Override
    public void reset() {

    }

    public void init(EffectEngine engine){
        isAlive = true;
        timeAlive = 0;
        engine.addEffect(this);
    }

    public void update (float timeDelta){
        timeAlive += timeDelta;
    }

    public abstract void draw(SpriteBatch batch, SizeEvaluator sizeEvaluator);

    public boolean isAlive() {
        return isAlive;
    }

    public abstract void release();
}
