package com.crazy.eki.com.graph.effects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Pool;
import com.crazy.eki.com.Constants;
import com.crazy.eki.com.graph.SizeEvaluator;
import com.crazy.eki.com.resources.Resources;

public class WarningEffect extends Effect{

   private int _fieldX;
   private int _fieldY;
   private Resources _resources;

   public static WarningEffect create(int fieldX,
                                      int fieldY,
                                      Resources resources,
                                      EffectEngine engine,
                                      WarningEffect.WarningEffectListener warningEffectListener){
       WarningEffect effect = warningEffectPool.obtain();

       effect.init(fieldX,fieldY,resources,engine,warningEffectListener);
       return effect;
   }

   public interface WarningEffectListener {
        void onEffectOver(WarningEffect effect);
   }

   private WarningEffectListener _warningEffectListener;

    public WarningEffect() {

    }

    public void init(int fieldX, int fieldY, Resources resources, EffectEngine engine, WarningEffectListener warningEffectListener){
        _fieldX = fieldX;
        _fieldY = fieldY;
        _resources = resources;
        _warningEffectListener = warningEffectListener;
        super.init(engine);
    }

    @Override
    public void update(float timeDelta) {
        super.update(timeDelta);
        if(timeAlive> Constants.WARNING_TIME){
           isAlive = false;
           if(_warningEffectListener!=null){
               _warningEffectListener.onEffectOver(this);
           }
        }
    }

    @Override
    public void draw(SpriteBatch batch,SizeEvaluator sizeEvaluator) {
       batch.begin();
       batch.draw(_resources.warning,sizeEvaluator.getBaseScreenX(_fieldX),sizeEvaluator.getBaseScreenY(_fieldY));
       batch.end();

    }

    @Override
    public void release() {
        warningEffectPool.free(this);
    }

    @Override
    public void reset() {
        super.reset();
    }

    static Pool<WarningEffect> warningEffectPool = new Pool<WarningEffect>(){
        @Override
        protected WarningEffect newObject() {
            return new WarningEffect();
        }
    };


    public int getFieldX(){
        return _fieldX;
    }

    public int get_fieldY(){
        return  _fieldY;
    }

}
