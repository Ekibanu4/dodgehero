package com.crazy.eki.com.graph.effects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.crazy.eki.com.graph.SizeEvaluator;

import java.util.*;

public class EffectEngine {

    List<Effect> effects;

    public EffectEngine() {
        effects = new ArrayList<Effect>();
    }

    public void update(float delta) {
        Iterator<Effect> i;
        i = effects.iterator();
        while (i.hasNext()) {
            Effect effect = i.next();
            if (!effect.isAlive) {
                effect.release();
                i.remove();
            } else {
                effect.update(delta);
            }
        }
    }

    public void addEffect(Effect effect) {
        effects.add(effect);
    }

    public void draw(SpriteBatch batch, SizeEvaluator sizeEvaluator) {
        for (Effect effect : effects) {
            effect.draw(batch, sizeEvaluator);
        }
    }

    public void clear() {
        if (effects.size() > 0) {
            for (Effect effect : effects) {
                effect.release();
            }
            effects.clear();
        }
    }
}
