package com.crazy.eki.com.graph;

import static com.crazy.eki.com.Constants.TILE_SIZE;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.crazy.eki.com.resources.Resources;

public class SizeEvaluator {
    private Stage _stage;
    private Resources _resources;

    private final int _maxTileBaseX;
    private final int _maxTileBaseY;
    private final int _margin = 4;
    private float _rightSideX;

    public SizeEvaluator(Stage stage, Resources resources, int maxTileBaseX, int maxTileBaseY, float rightSideX) {
        _stage = stage;
        _resources = resources;
        _maxTileBaseX = maxTileBaseX;
        _maxTileBaseY = maxTileBaseY;
        _rightSideX = rightSideX;
    }

    public float getBaseScreenX(int baseX) {
        return _stage.getWidth() / 2
                - (TILE_SIZE + _margin) *
                (_maxTileBaseX + 1 - baseX);
    }

    public float getBaseScreenY(int baseY) {
        return _stage.getHeight() / 2
                - (TILE_SIZE + _margin) * (_maxTileBaseY + 1 - baseY) + ((TILE_SIZE + _margin) * _maxTileBaseY) / 2;
    }

    public float getEnemyX(Sprite sprite) {
        return ((_stage.getWidth() * 3) / 4) - (sprite.getWidth() / 2);
    }

    public float getEnemyY(Sprite sprite) {
        return (_stage.getHeight() / 2) - (sprite.getHeight() / 2);
    }

    public float getRightSideX() {
        return _rightSideX;
    }

    public void setRightSideX(float rightSideX) {
        _rightSideX = rightSideX;
    }
}
