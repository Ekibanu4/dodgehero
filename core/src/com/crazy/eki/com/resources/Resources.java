package com.crazy.eki.com.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Resources {
    TextureAtlas gameSprites;

    //-------------Fonts----------------

    public BitmapFont gameFont;

    //-------------Textures-------------
    public TextureRegion grass;
    public TextureRegion rock;
    public TextureRegion sand;
    public TextureRegion deepWater;
    public TextureRegion warning;
    public TextureRegion base;
    public Sprite hero;
    public Sprite enemy;
    public Sprite attackBonus;
    public Sprite healBonus;

    public Resources() {

        gameFont = new BitmapFont(Gdx.files.internal("fonts/gamefont.fnt"),Gdx.files.internal("fonts/gamefont.png"),false);

        gameSprites = new TextureAtlas(Gdx.files.internal("packed/game_assets.atlas"));
        grass = gameSprites.findRegion("grass");
        rock = gameSprites.findRegion("rock");
        sand = gameSprites.findRegion("sand");
        deepWater = gameSprites.findRegion("deep_water");
        hero = new Sprite(gameSprites.findRegion("hero"));
        enemy = new Sprite(gameSprites.findRegion("clay_golem"));
        base = gameSprites.findRegion("base");
        warning = gameSprites.findRegion("warning");
        attackBonus = new Sprite(gameSprites.findRegion("sword_bonus"));
        healBonus = new Sprite(gameSprites.findRegion("hart"));
    }

    public void dispose() {
        gameSprites.dispose();
    }

}
