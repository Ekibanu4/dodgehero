package com.crazy.eki.com.logic;

import static com.crazy.eki.com.Constants.BONUS_SPAWN_TIME;
import static com.crazy.eki.com.Constants.MAX_BASE_X;
import static com.crazy.eki.com.Constants.MAX_BASE_Y;
import static com.crazy.eki.com.Constants.MAX_BONUSES_AT_FIELD;
import static com.crazy.eki.com.logic.objects.bonus.BaseBonus.BONUS_TYPE_ATTACK;
import static com.crazy.eki.com.logic.objects.bonus.BaseBonus.BONUS_TYPE_HEAL;

import com.badlogic.gdx.math.MathUtils;
import com.crazy.eki.com.Constants;
import com.crazy.eki.com.DodgeHero;
import com.crazy.eki.com.graph.effects.EffectEngine;
import com.crazy.eki.com.graph.effects.WarningEffect;
import com.crazy.eki.com.logic.objects.Enemy;
import com.crazy.eki.com.logic.objects.Player;
import com.crazy.eki.com.logic.objects.bonus.BaseBonus;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GameLogic implements Enemy.EnemyAttackListener, WarningEffect.WarningEffectListener {

    private Player _player;
    private Enemy _enemy;
    private EffectEngine _engine;
    private DodgeHero _game;

    private List<BaseBonus> _bonuses;
    float _bonusTimePastFromLastBonus;
    float _gameTime;
    private GameEventListener _gameEventListener;

    public interface GameEventListener{
        void gameWin(boolean isWin);
    }

    public GameLogic(DodgeHero game,GameEventListener gameEventListener) {
        _game = game;
        _gameEventListener = gameEventListener;
        _player = new Player(
                MathUtils.random(MAX_BASE_X),
                MathUtils.random(MAX_BASE_Y),
                Constants.PLAYER_HIT_POINTS,
                _game.resources
        );
        _enemy = new Enemy(_game.resources, this);
        _engine = new EffectEngine();

        _bonuses = new ArrayList<>();
        _bonusTimePastFromLastBonus = 0;
        _gameTime = 0;
    }

    public Player getPlayer() {
        return _player;
    }

    public Enemy getEnemy() {
        return _enemy;
    }

    public void update(float timeDelta) {
        _gameTime += timeDelta;
        _player.update(timeDelta);

        if (_player.getLives() > 0 &&
                _enemy.getLives() > 0) {
            _engine.update(timeDelta);
            _enemy.update(timeDelta);
            if (_bonusTimePastFromLastBonus + BONUS_SPAWN_TIME <= _gameTime &&
                    _bonuses.size() < MAX_BONUSES_AT_FIELD) {
                spawnRandomBonus();
            }
        }
    }

    private void spawnRandomBonus() {
        int fx = 0;
        int fy = 0;
        boolean targetNotEmpty = true;
        do {
            fx = MathUtils.random(MAX_BASE_X);
            fy = MathUtils.random(MAX_BASE_Y);
            targetNotEmpty = _player.getFieldX() == fx && _player.getFieldY() == fy;
            for (int i = 0; i < _bonuses.size() && targetNotEmpty; i++) {
                targetNotEmpty = (_bonuses.get(i).getFieldY() != fy && _bonuses.get(i).getFieldX() != fx);
            }
        } while (targetNotEmpty);
        _bonuses.add(BaseBonus.create(fx, fy, MathUtils.random(100) < 25 ? BONUS_TYPE_HEAL : BONUS_TYPE_ATTACK, _game.resources));
        _bonusTimePastFromLastBonus = _gameTime;
    }

    public boolean canMove(int newX, int newY) {
        return newX >= 0 && newX <= MAX_BASE_X && newY >= 0 && newY <= MAX_BASE_Y && _enemy.getLives() > 0;
    }

    public void setNewPlayerPosition(int newX, int newY) {
        _player.setFieldX(newX);
        _player.setFieldY(newY);
        checkBonusesCollected();
        checkWinConditions();
    }

    private void checkWinConditions() {
        if (_enemy.getLives() == 0) {
          _player.setWin();
          if(_gameEventListener!=null){
              _engine.clear();
              _gameEventListener.gameWin(true);
          }
        }
    }

    private void checkBonusesCollected() {
        Iterator<BaseBonus> i;
        i = _bonuses.iterator();
        while (i.hasNext()) {
            final BaseBonus baseBonus = i.next();
            if (baseBonus.getFieldX() == _player.getFieldX() &&
                    baseBonus.getFieldY() == _player.getFieldY()) {
                switch (baseBonus.getBonusType()) {
                    case BONUS_TYPE_ATTACK:
                        _enemy.takeDamage(getPlayer().getDamageAmount());
                        break;
                    case BONUS_TYPE_HEAL:
                        getPlayer().heal(baseBonus.getHealthImpact());
                        break;
                    default: {
                    }
                }
                baseBonus.release();
                i.remove();
            }
        }
    }

    public EffectEngine getEngine() {
        return _engine;
    }

    @Override
    public void OnAttack(boolean[][] tiles) {
        for (int x = 0; x < tiles.length; x++) {
            for (int y = 0; y < tiles[x].length; y++) {
                if (tiles[x][y])
                    WarningEffect.create(x, y, _game.resources, _engine, this);
            }
        }
    }

    @Override
    public void onEffectOver(WarningEffect effect) {
        if (_player.getFieldX() == effect.getFieldX() &&
                _player.getFieldY() == effect.get_fieldY()) {
            _player.takeDamage(_enemy.getDamageAmount());
        }
    }

    public List<BaseBonus> getBonuses() {
        return _bonuses;
    }
}
