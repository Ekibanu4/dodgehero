package com.crazy.eki.com.logic.objects;

import static com.crazy.eki.com.Constants.BASE_PLAYER_DAMAGE;
import static com.crazy.eki.com.Constants.DEFAULT_PLAYER_APPROACH_TIME;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.crazy.eki.com.graph.SizeEvaluator;
import com.crazy.eki.com.logic.objects.bonus.Character;
import com.crazy.eki.com.resources.Resources;

public class Player extends Character {

    private int _fieldX;
    private int _fieldY;

    private boolean _wining = false;
    private float _winTime = -1;

    public Player(int fieldX,
                  int fieldY,
                  int lifePoints,
                  Resources resources) {
        super(lifePoints, BASE_PLAYER_DAMAGE);
        set(resources.hero);
        _fieldX = fieldX;
        _fieldY = fieldY;
    }

    public void draw(SpriteBatch batch, SizeEvaluator sizeEvaluator) {
        preDraw();
        if (_timeAlive < DEFAULT_PLAYER_APPROACH_TIME) {
            float t = _timeAlive / DEFAULT_PLAYER_APPROACH_TIME;
            t = t * t;
            setPosition(t * sizeEvaluator.getBaseScreenX(_fieldX), sizeEvaluator.getBaseScreenY(_fieldY));
        } else if(_wining){
            float t = 1;
            if(_timeAlive - _winTime <DEFAULT_PLAYER_APPROACH_TIME){
                t = (_timeAlive -_winTime)/ DEFAULT_PLAYER_APPROACH_TIME;
                t = t * t;
                float fx = sizeEvaluator.getBaseScreenX(_fieldX);
                setPosition(fx+t*(sizeEvaluator.getRightSideX()), sizeEvaluator.getBaseScreenY(_fieldY));
            }
        }else {
            setPosition(sizeEvaluator.getBaseScreenX(_fieldX), sizeEvaluator.getBaseScreenY(_fieldY));
        }
        super.draw(batch);
        postDraw();
    }

    public int getFieldX() {
        return _fieldX;
    }

    public void setFieldX(int _fieldX) {
        this._fieldX = _fieldX;
    }

    public int getFieldY() {
        return _fieldY;
    }

    public void setFieldY(int _fieldY) {
        this._fieldY = _fieldY;
    }

    @Override
    public void update(float timeDelta) {
        super.update(timeDelta);
    }

    public void setWin() {
        _wining = true;
        _winTime = _timeAlive;
    }
}
