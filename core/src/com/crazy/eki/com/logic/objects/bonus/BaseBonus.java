package com.crazy.eki.com.logic.objects.bonus;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.utils.Pool;
import com.crazy.eki.com.graph.SizeEvaluator;
import com.crazy.eki.com.resources.Resources;

public class BaseBonus extends Sprite implements Pool.Poolable {

    public static final byte BONUS_TYPE_ATTACK = 0;
    public static final byte BONUS_TYPE_HEAL = 1;


    private  int _fieldX;
    private  int _fieldY;

    private byte _bonusType;
    private int _healthImpact;


    public BaseBonus() {
    }

    public void setup (int fieldX, int fieldY, byte bonusType, Resources resources) {
        _fieldX = fieldX;
        _fieldY = fieldY;
        _bonusType = bonusType;
        if(_bonusType==BONUS_TYPE_ATTACK){
            _healthImpact = 0;
            set(resources.attackBonus);
        }
        if(_bonusType==BONUS_TYPE_HEAL){
            _healthImpact = 1;
            set(resources.healBonus);
        }
    }

    @Override
    public void reset() {

    }

    static final Pool<BaseBonus> bonusPoll = new Pool<BaseBonus>() {
        @Override
        protected BaseBonus newObject() {
            return new BaseBonus();
        }
    };

    public static BaseBonus create(int fieldX,int fieldY,byte bonusType , Resources resources){
        BaseBonus bonus = bonusPoll.obtain();
        bonus.setup(fieldX,fieldY,bonusType,resources);
        return bonus;
    }

    public void draw(SpriteBatch batch, SizeEvaluator sizeEvaluator){
        setPosition(sizeEvaluator.getBaseScreenX(_fieldX),sizeEvaluator.getBaseScreenY(_fieldY));
        super.draw(batch);
    }

    public int getFieldX() {
        return _fieldX;
    }

    public int getFieldY() {
        return _fieldY;
    }

    public byte getBonusType() {
        return _bonusType;
    }

    public int getHealthImpact() {
        return _healthImpact;
    }

    public void release(){
        bonusPoll.free(this);
    }
}
