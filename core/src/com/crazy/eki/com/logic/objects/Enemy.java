package com.crazy.eki.com.logic.objects;

import static com.crazy.eki.com.Constants.DEFAULT_ENEMY_DAMAGE;
import static com.crazy.eki.com.Constants.DEFAULT_ENEMY_LIVES;
import static com.crazy.eki.com.Constants.DEFAULT_PLAYER_APPROACH_TIME;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.crazy.eki.com.Constants;
import com.crazy.eki.com.graph.SizeEvaluator;
import com.crazy.eki.com.logic.objects.bonus.Character;
import com.crazy.eki.com.resources.Resources;

public class Enemy extends Character {

    private float _timeSinceLastAttack;
    private float _timeNextAttack;
    private boolean _targetTiles[][];

    public Enemy(Resources resources, EnemyAttackListener enemyAttackListener) {
        super(DEFAULT_ENEMY_LIVES, DEFAULT_ENEMY_DAMAGE);
        set(resources.enemy);
        resetAttackTime();
        _attackListener = enemyAttackListener;
        _targetTiles = new boolean[Constants.MAX_BASE_X + 1][];
        for (int i = 0; i < Constants.MAX_BASE_Y + 1; i++) {
            _targetTiles[i] = new boolean[Constants.MAX_BASE_Y + 1];
        }
    }

    public interface EnemyAttackListener {
        void OnAttack(boolean[][] tiles);
    }

    private EnemyAttackListener _attackListener;

    private void resetAttackTime() {
        _timeSinceLastAttack = 0;
        _timeNextAttack = Constants.BASE_ATTACK_TIME + MathUtils.random(2);
    }

    public void draw(SpriteBatch batch, SizeEvaluator sizeEvaluator) {
        preDraw();
        if (_timeAlive < DEFAULT_PLAYER_APPROACH_TIME) {
            float t = _timeAlive / DEFAULT_PLAYER_APPROACH_TIME;
            t = t * t;
            setScale(t);
        } else {
            setScale(1);
        }
        setPosition(sizeEvaluator.getEnemyX(this), sizeEvaluator.getEnemyY(this));
        super.draw(batch);
        postDraw();
    }

    public void update(float deltaTime) {
        super.update(deltaTime);
        _timeSinceLastAttack += deltaTime;
        if (_timeSinceLastAttack > _timeNextAttack) {
            int coll1 = MathUtils.random(Constants.MAX_BASE_X);
            int coll2 = 0;
            do {
                coll2 = MathUtils.random(Constants.MAX_BASE_X);
            } while (coll1 == coll2);
            for (int x = 0; x < Constants.MAX_BASE_X + 1; x++) {
                for (int y = 0; y < Constants.MAX_BASE_Y + 1; y++) {
                    _targetTiles[x][y] = (x == coll1 || x == coll2);
                }
            }
            _attackListener.OnAttack(_targetTiles);
            _timeSinceLastAttack = 0;
        }
    }

}
