package com.crazy.eki.com.logic.objects.bonus;

import static com.crazy.eki.com.Constants.BLINKING_CHARACTER_TIME;

import com.badlogic.gdx.graphics.g2d.Sprite;

public abstract class Character extends Sprite {
    protected int _lives;
    protected int _damageAmount;

    protected  float _timeAlive;
    private float _takingDamageTime;

    public Character(int lives,int damageAmount) {
        _lives = lives;
        _damageAmount = damageAmount;
        _timeAlive = 0;
        _takingDamageTime = -1;
    }

    public int getDamageAmount() {
        return _damageAmount;
    }

    public int getLives() {
        return _lives;
    }

    public void takeDamage(int damage) {
        _takingDamageTime = _timeAlive;
        _lives -= damage;
        if (_lives < 0) {
            _lives = 0;
        }
    }

    public void heal(int healAmount) {
        _lives += healAmount;
    }

    public void update(float timeDelta){
        _timeAlive +=timeDelta;
    }

    public void preDraw(){
        if(_timeAlive<_takingDamageTime+BLINKING_CHARACTER_TIME){
            float t = (_timeAlive - _takingDamageTime) / BLINKING_CHARACTER_TIME;
            t = t * t;
            setColor(1,1,1,t);
        }
    }

    public void postDraw(){
        setColor(1,1,1,1);
    }

    public float getTakingDamageTime() {
        return _takingDamageTime;
    }

    public float getTimeAlive() {
        return _timeAlive;
    }
}
