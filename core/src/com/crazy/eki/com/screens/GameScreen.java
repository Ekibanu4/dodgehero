package com.crazy.eki.com.screens;

import static com.crazy.eki.com.Constants.MAX_BASE_X;
import static com.crazy.eki.com.Constants.MAX_BASE_Y;
import static com.crazy.eki.com.Constants.SCREEN_HEIGHT;
import static com.crazy.eki.com.Constants.SCREEN_WIDTH;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.crazy.eki.com.DodgeHero;
import com.crazy.eki.com.graph.Background;
import com.crazy.eki.com.graph.SizeEvaluator;
import com.crazy.eki.com.logic.GameLogic;
import com.crazy.eki.com.logic.objects.Player;
import com.crazy.eki.com.logic.objects.bonus.BaseBonus;

public class GameScreen extends BaseScreen implements InputProcessor, GameLogic.GameEventListener {

    SpriteBatch batch;
    private Stage _gameStage;
    private Background _background;
    private SizeEvaluator _sizeEvaluator;
    private GameLogic _gameLogic;
    private Player _playerInstance;

    private static final float SHAKE_SCREEN_ON_DAMAGE = 0.3f;
    private static final int SHAKE_DISTANCE = 4;
    private static final float FADE_TIME = 0.5f;
    private float fadeStartTime;

    public GameScreen(DodgeHero game) {
        super(game);
        batch = new SpriteBatch();

        ExtendViewport viewport = new ExtendViewport(SCREEN_WIDTH, SCREEN_HEIGHT);
        _gameStage = new Stage(viewport, batch);
        _background = new Background();
        _sizeEvaluator = new SizeEvaluator(_gameStage, _game.resources, MAX_BASE_X, MAX_BASE_Y, _gameStage.getWidth());
        _gameLogic = new GameLogic(_game, this);
        _playerInstance = _gameLogic.getPlayer();
        fadeStartTime = 0;
        Gdx.input.setInputProcessor(this);

        _gameStage.addAction(new Action() {
            float time = 0;

            @Override
            public boolean act(float delta) {
                time += delta;
                float t = time / FADE_TIME;
                t *= t;
                batch.setColor(1, 1, 1, t <= 1 ? t : 1);
                return time >= FADE_TIME;
            }
        });
    }

    public void update(float delta) {
        _gameStage.act(delta);
        _gameLogic.update(delta);
    }

    private void drawUI() {
        batch.begin();
        _game.resources.gameFont.setColor(Color.BLACK);
        drawShadowedText("Lives:" + _playerInstance.getLives(),
                10,
                _gameStage.getViewport().getScreenY() + _gameStage.getHeight() - 10,
                _gameStage.getWidth(),
                Align.left,
                Color.WHITE);

        drawShadowedText("Enemy:" + _gameLogic.getEnemy().getLives(),
                _gameStage.getWidth() / 2 + 10,
                _gameStage.getViewport().getScreenY() + _gameStage.getHeight() - 10,
                _gameStage.getWidth() / 2 - 10,
                Align.left, Color.WHITE);

        if (_playerInstance.getLives() == 0) {
            showGameResult("DEFEAT!", Color.RED);
        }

        if (_gameLogic.getEnemy().getLives() == 0) {
            showGameResult("VICTORY!", Color.GREEN);
        }

        batch.end();
    }

    private void showGameResult(String message, Color color) {
        drawShadowedText(message, 0, _gameStage.getViewport().getScreenY() + _gameStage.getHeight() / 2, _gameStage.getWidth(), Align.center, color);
    }

    private void drawShadowedText(String s,
                                  float x,
                                  float y,
                                  float width,
                                  int align,
                                  Color color) {
        _game.resources.gameFont.setColor(Color.BLACK);
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                _game.resources.gameFont.draw(batch, s, x + i, y + j, _gameStage.getWidth(), align, false);
            }
        }

        _game.resources.gameFont.setColor(color);
        _game.resources.gameFont.draw(batch, s, x, y, width, align, false);
        _game.resources.gameFont.setColor(Color.WHITE);

    }

    @Override
    public void render(float delta) {
        super.render(delta);
        update(delta);
        ScreenUtils.clear(0, 0, 0, 1);
        _background.draw(_gameStage, _game.resources);
        drawBase();
        _gameLogic.getEngine().draw(batch, _sizeEvaluator);
        batch.begin();
        for (BaseBonus bonus : _gameLogic.getBonuses()) {
            bonus.draw(batch, _sizeEvaluator);
        }
        _playerInstance.draw(batch, _sizeEvaluator);
        _gameLogic.getEnemy().draw(batch, _sizeEvaluator);
        batch.end();
        _gameStage.getCamera().position.set(_gameStage.getWidth() / 2, _gameStage.getHeight() / 2, 0);
        if (_playerInstance.getLives() > 0 &&
                _playerInstance.getTimeAlive() - _playerInstance.getTakingDamageTime() < SHAKE_SCREEN_ON_DAMAGE) {
            _gameStage.getCamera().translate(-(SHAKE_DISTANCE / 2) + MathUtils.random(SHAKE_DISTANCE),
                    -(SHAKE_DISTANCE / 2) + MathUtils.random(SHAKE_DISTANCE), 0);
        }
        drawUI();
        _gameStage.draw();

    }

    private void drawBase() {
        batch.begin();
        for (int x = 0; x <= MAX_BASE_X; x++) {
            for (int y = 0; y <= MAX_BASE_Y; y++) {

                batch.draw(_game.resources.base, _sizeEvaluator.getBaseScreenX(x),
                        _sizeEvaluator.getBaseScreenY(y));

            }
        }
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
        Gdx.input.setInputProcessor(null);
        super.dispose();
    }

    public void attemptMove(int dX, int dY) {
        if (_gameLogic.canMove(_playerInstance.getFieldX() + dX, _playerInstance.getFieldY() + dY)) {
            _gameLogic.setNewPlayerPosition(_playerInstance.getFieldX() + dX, _playerInstance.getFieldY() + dY);
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        _gameStage.getViewport().update(width, height, true);
        _sizeEvaluator.setRightSideX(_gameStage.getWidth());
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.UP:
                attemptMove(0, 1);
                break;
            case Input.Keys.DOWN:
                attemptMove(0, -1);
                break;
            case Input.Keys.LEFT:
                attemptMove(-1, 0);
                break;
            case Input.Keys.RIGHT:
                attemptMove(1, 0);
                break;

        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }

    @Override
    public void gameWin(boolean isWin) {
        _gameStage.addAction(Actions.sequence(
                new Action() {
                    float time = 0;

                    @Override
                    public boolean act(float delta) {
                        time += delta;
                        float t =   time/FADE_TIME  ;
                        t = t * t;
                        batch.setColor(1, 1, 1, 1-t);
                        return time >= FADE_TIME;
                    }
                },
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        dispose();
                        _game.setScreen(new GameScreen(_game));
                        return true;
                    }
                }
        ));
    }
}

