package com.crazy.eki.com;

import com.badlogic.gdx.Game;
import com.crazy.eki.com.resources.Resources;
import com.crazy.eki.com.screens.GameScreen;

public class DodgeHero extends Game {

  public Resources resources;

    @Override
    public void create() {
        resources = new Resources();
        setScreen(new GameScreen(this));
    }

    @Override
    public void dispose() {
        resources.dispose();
    }
}
